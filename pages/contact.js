import Nav from '../components/nav'
import Header from '../components/header'

export default function IndexPage() {
  return (
    <div>


      <Header />
      <Nav />

    </div>
  )
}
