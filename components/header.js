export default function Header() {
  return (
    <section className="flex-col items-center bg-pink-500 py-5">
      <h1 className="text-3xl text-center text-gray-100 font-bold tracking-tighter leading-tight">
        আলাপ
      </h1>
      <h4 className="text-center -mt-1 text-gray-100 text-lg">
        বিজ্ঞান পরিচয়
      </h4>
    </section>
  )
}