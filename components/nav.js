export default function Nav() {
  return (

<nav className="bg-white px-8 pt-2 rounded-lg shadow-md">
     <div className="-mb-px flex justify-between">
<a className="no-underline text-pink-500 border-b-2 border-transparent tracking-wide font-bold py-3">
             পরিচয়
         </a>
<a className="no-underline text-pink-500 border-b-2 border-transparent tracking-wide font-bold py-3">
             ব্লগ
         </a>
<a className="no-underline text-pink-500 border-b-2 border-transparent tracking-wide font-bold py-3">
             সদস্য
         </a>
<a className="no-underline text-pink-500 border-b-2 border-transparent tracking-wide font-bold py-3">
             যোগাযোগ
         </a>
     </div>
</nav>
  )
}
